import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest extends Validator {

    @Test
    void testValidateHeader() {
        assertTrue(validateHeader("S123456789"));
        assertTrue(validateHeader("T123456789"));
        assertTrue(validateHeader("E123456789"));
        assertTrue(validateHeader("E123356886"));
        assertFalse(validateHeader("s123456789"));
        assertFalse(validateHeader("E12345678"));
        assertFalse(validateHeader("t123456789"));
        assertFalse(validateHeader("S1234567891"));
    }

    @Test
    void testValidateSmsBody() {
        String sender = "1234";
        String message = "Hello";
        String badSender = "Hello";
        String badMessage = "This message will be more than 140 characters. This message will be more than 140 characters. This message will be more than 140 characters. This message will be more than 140 characters.";

        String[] toSucceed = {sender, message};
        String[] toFail = {badSender, badMessage};
        String[] singleFailure = {badSender, message};

        assertTrue(validateSmsBody(toSucceed));
        assertFalse(validateSmsBody(toFail));
        assertFalse(validateSmsBody(singleFailure));
    }

    @Test
    void testValidateEmailBody() {
        String sender = "test@example.com";
        String subject = "Hello";
        String message = "Hello there";
        String badSender = "test@example";

        String[] toSucceed = {sender, subject, message};
        String[] toFail = {badSender, subject, message};

        assertTrue(validateEmailBody(toSucceed));
        assertFalse(validateEmailBody(toFail));
    }

    @Test
    void testValidateTweetBody() {
        String sender = "@example";
        String message = "Hello there";
        String badSender = "test@example";
        String badMessage = "This message will be more than 140 characters. This message will be more than 140 characters. This message will be more than 140 characters. This message will be more than 140 characters.";

        String[] toSucceed = {sender, message};
        String[] toFail = {badSender, badMessage};

        assertTrue(validateTweetBody(toSucceed));
        assertFalse(validateTweetBody(toFail));
    }
}