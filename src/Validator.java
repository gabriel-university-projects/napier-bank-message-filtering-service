import java.util.regex.*;

public class Validator {

    // Constant declaration

    private static final String HEADER_PATTERN = "[A-Z]{1}\\d{9}";
    private static final String FIRST_WORD_SMS = "\\d+";
    private static final String FIRST_WORD_EMAIL = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
    private static final String FIRST_WORD_TWEET = "@[a-zA-Z0-9_]{0,15}";
    private static final int SMS_REMAINDER_LENGTH = 140;
    private static final int EMAIL_SUBJECT_LENGTH = 20;
    private static final int EMAIL_MESSAGE_LENGTH = 1028;
    private static final int TWEET_REMAINDER_LENGTH = 140;

    // Variable declaration

    private int expectedCategoryId;


    // Getters

    public int getExpectedCategoryId() {
        return this.expectedCategoryId;
    }

    // Setters

    public void setExpectedCategoryId(int id) {
        this.expectedCategoryId = id;
    }

    // Functional Methods

    public boolean validateHeader(String header) {
        if (this.isSms(header) || this.isEmail(header) || this.isTweet(header)) {
            return Pattern.matches(HEADER_PATTERN, header);
        }
        return false;
    }

    public boolean validateBody(String[] body) {
        if (body.length > 3) {
            return false;
        }
        else {
            switch(this.getExpectedCategoryId()) {
                case 1:
                    return this.validateSmsBody(body);
                case 2:
                    return this.validateEmailBody(body);
                case 3:
                    return this.validateTweetBody(body);
                default:
                    return false;
            }
        }
    }

    private boolean isSms(String header) {
        if (header.startsWith("S")) {
            this.setExpectedCategoryId(1);
            return true;
        }
        else {
            return false;
        }
    }

    private boolean isEmail(String header) {
        if (header.startsWith("E")) {
            this.setExpectedCategoryId(2);
            return true;
        }
        else {
            return false;
        }
    }

    private boolean isTweet(String header) {
        if (header.startsWith("T")) {
            this.setExpectedCategoryId(3);
            return true;
        }
        else {
            return false;
        }
    }

    public boolean validateSmsBody(String[] body) {
        if (body.length == 2) {
            String number = body[0];
            String message = body[1];

            if (Pattern.matches(FIRST_WORD_SMS, number)) {
                return message.length() <= SMS_REMAINDER_LENGTH;
            }
        }

        return false;
    }

    public boolean validateEmailBody(String[] body) {
        if (body.length == 3) {
            String emailAddress = body[0];
            String subject = body[1];
            String message = body[2];

            if (Pattern.matches(FIRST_WORD_EMAIL, emailAddress)) {
                if (subject.length() <= EMAIL_SUBJECT_LENGTH) {
                    return message.length() <= EMAIL_MESSAGE_LENGTH;
                }
            }
        }

        return false;
    }

    public boolean validateTweetBody(String[] body) {
        if (body.length == 2) {
            String handle = body[0];
            String message = body[1];

            if (Pattern.matches(FIRST_WORD_TWEET, handle)) {
                return message.length() <= TWEET_REMAINDER_LENGTH;
            }
        }

        return false;
    }
}
