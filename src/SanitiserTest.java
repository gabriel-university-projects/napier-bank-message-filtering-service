import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SanitiserTest extends Sanitiser {

    @Test
    void testSanitise() {
        String input = "Hello | Test | Example";
        String[] expectedOutput = {"Hello", "Test", "Example"};
        String[] actualOutput = sanitise(input);

        assertEquals(expectedOutput[0], actualOutput[0]);
        assertEquals(expectedOutput[1], actualOutput[1]);
        assertEquals(expectedOutput[2], actualOutput[2]);
    }

    @Test
    void testConvertTts() {
        String input = "Hello, it was a great day and AAP to see you";
        String expectedOutput = "Hello, it was a great day and <Always a pleasure> to see you";

        assertEquals(convertTts(input), expectedOutput);
    }
}