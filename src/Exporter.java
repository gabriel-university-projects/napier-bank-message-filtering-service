import org.json.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Exporter {

    private String message;
    private String fileName;

    // Getters

    public String getMessage() {
        return this.message;
    }
    public String getFileName() {
        return this.fileName;
    }

    // Setters

    public void setMessage(String message) {
        this.message = message;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    // Functional Methods

    public void export(String header, String[] body, int category) {
        this.parse(header, body, category);
        this.exportToFile(message);
    }

    private void parse(String header, String[] body, int category) {
        Categoriser categoriser = new Categoriser();

        JSONObject jsonObject = new JSONObject();

        JSONArray messageArray = new JSONArray();
        JSONObject messageElement = new JSONObject();

        messageElement.put("categoryId", category);
        messageElement.put("categoryName", categoriser.getCategoryName(category));
        messageElement.put("header", header);

        JSONObject messageBody = new JSONObject();
        messageBody.put("sender", body[0]);

        if (category == 2) {
            messageBody.put("subject", body[1]);
            messageBody.put("content", body[2]);
        }
        else {
            messageBody.put("content", body[1]);
        }

        messageElement.put("body", messageBody);
        messageArray.put(messageElement);

        jsonObject.put("message", messageArray);

        this.setMessage(jsonObject.toString());
    }

    private void exportToFile(String toExport) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime now = LocalDateTime.now();
        String timeStamp = dtf.format(now);

        this.setFileName("messageExport" + timeStamp + ".json");

        File file = new File(fileName);

        try {
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(toExport);
            fileWriter.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
