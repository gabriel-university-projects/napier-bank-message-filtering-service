import javax.swing.*;
import java.io.IOException;

public class MessageFacade {

    // Global Objects

    JFrame frame = new JFrame("Napier Bank Message Filtering Service");

    // Variable declaration

    private String[] messageBodyArray;
    private int messageCategory;

    // Getters

    public String[] getMessageBodyArray() {
        return this.messageBodyArray;
    }
    public int getMessageCategory() {
        return this.messageCategory;
    }

    // Setters

    public void setMessageBodyArray(String[] input) {
        this.messageBodyArray = input;
    }
    public void setMessageCategory(int input) {
        this.messageCategory = input;
    }

    // Functional methods

    public void run() {
        frame.setContentPane(new GuiInterface().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void submitFromFile() throws IOException {
        Importer importer = new Importer();
        importer.readFromFile();
        this.manageSubmission(importer.getHeader(), importer.getBody());
    }

    public void manageSubmission(String messageHeader, String messageBody) {
        Sanitiser sanitiser = new Sanitiser();
        this.setMessageBodyArray(sanitiser.sanitise(messageBody));

        Validator validator = new Validator();

        if (!validator.validateHeader(messageHeader)) {
            JOptionPane.showMessageDialog(null, "Your message header is invalid!");
        }
        else if (!validator.validateBody(messageBodyArray)) {
            JOptionPane.showMessageDialog(null, "Your message body is invalid!");
        }
        else {
            this.setMessageCategory(validator.getExpectedCategoryId());

            Categoriser categoriser = new Categoriser();
            categoriser.setCategoryId(this.getMessageCategory());

            if (this.getMessageCategory() != 2) {
                messageBodyArray[1] = sanitiser.convertTts(messageBodyArray[1]);
            }

            if (this.getMessageCategory() == 2) {
                messageBodyArray[2] = sanitiser.removeUrl(messageBodyArray[2]);
            }

            Formatter formatter = new Formatter();
            String finalMessage = formatter.formatMessage(messageHeader, messageBodyArray, categoriser.getCategoryId());

            JOptionPane.showMessageDialog(null, finalMessage);

            Exporter exporter = new Exporter();
            exporter.export(messageHeader, messageBodyArray, categoriser.getCategoryId());

            JOptionPane.showMessageDialog(null, "Your message has been exported to file:\n" + exporter.getFileName());
        }
    }
}
