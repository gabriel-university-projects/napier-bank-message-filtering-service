public class Formatter {

    // Variable declaration

    private String wholeMessageOutput;

    // Getters
    
    public String getWholeMessageOutput() {
        return this.wholeMessageOutput;
    }

    // Setters

    public void setWholeMessageOutput(String input) {
        this.wholeMessageOutput = input;
    }

    // Functional methods

    public String formatMessage(String header, String[] body, int categoryId) {
        switch (categoryId) {
            case 1:
                formatSms(header, body);
                break;
            case 2:
                formatEmail(header, body);
                break;
            case 3:
                formatTweet(header, body);
                break;
        }

        return this.getWholeMessageOutput();
    }

    private void formatSms(String header, String[] body) {
        String sender = body[0];
        String message = body[1];

        this.setWholeMessageOutput(
                "This message is an SMS\n\n" +
                        "Message Header: " + header + "\n" +
                        "Message Sender: " + sender + "\n" +
                        "Message Content: " + message
        );
    }

    private void formatEmail(String header, String[] body) {
        String sender = body[0];
        String subject = body[1];
        String message = body[2];

        this.setWholeMessageOutput(
                "This message is an email\n\n" +
                        "Message Header: " + header + "\n" +
                        "Message Sender: " + sender + "\n" +
                        "Message Subject: " + subject + "\n" +
                        "Message Content: " + message
        );

    }

    private void formatTweet(String header, String[] body) {
        String handle = body[0];
        String message = body[1];

        this.setWholeMessageOutput(
                "This message is a Tweet\n\n" +
                        "Message Header: " + header + "\n" +
                        "Message Sender Handle: " + handle + "\n" +
                        "Message Content: " + message
        );
    }

}
