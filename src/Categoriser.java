public class Categoriser {

    // Variable declaration

    private int categoryId;

    // Getters

    public int getCategoryId() {
        return this.categoryId;
    }

    // Setters

    public void setCategoryId(int input) {
        this.categoryId = input;
    }

    // Functional Methods

    public String getCategoryName(int categoryId) {
        switch(categoryId) {
            case 1:
                return "SMS";
            case 2:
                return "Email";
            case 3:
                return "Tweet";
            default:
                return "";
        }
    }

}
