import javax.swing.*;
import java.io.IOException;

public class GuiInterface {

    MessageFacade messageFacade = new MessageFacade();

    public JPanel panelMain;
    public JButton submit_button;
    public JTextArea messageHeader;
    public JTextArea messageBody;
    public JLabel messageBodyLabel;
    private JButton read_from_file_button;

    String messageHeaderString;
    String messageBodyString;

    public String getMessageHeaderString() {
        return this.messageHeaderString;
    }
    public String getMessageBodyString() {
        return this.messageBodyString;
    }
    public void setMessageHeaderString(String input) {
        this.messageHeaderString = input;
    }
    public void setMessageBodyString(String input) {
        this.messageBodyString = input;
    }

    private void collectAndRun() {
        this.setMessageHeaderString(messageHeader.getText());
        this.setMessageBodyString(messageBody.getText());

        messageFacade.manageSubmission(this.getMessageHeaderString(), this.getMessageBodyString());
    }

    public GuiInterface() {
        submit_button.addActionListener(e -> this.collectAndRun());
        read_from_file_button.addActionListener(e -> {
            try {
                messageFacade.submitFromFile();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null,"File not found! Please add \"message.txt\" to the directory of this program");
                ex.printStackTrace();
            }
        });
    }
}
