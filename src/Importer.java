import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;

public class Importer {

    private String header;
    private String body;

    public String getHeader() {
        return this.header;
    }
    public String getBody() {
        return this.body;
    }

    public void setHeader(String header) {
        this.header = header;
    }
    public void setBody(String body) {
        this.body = body;
    }

    public void readFromFile() throws IOException {

        byte[] fileContents = Files.readAllBytes(Paths.get("message.txt"));
        String rawContents = new String(fileContents, StandardCharsets.UTF_8);

        Sanitiser sanitiser = new Sanitiser();
        String[] sanitisedArray = sanitiser.splitByDoublePipe(rawContents);

        this.setHeader(sanitisedArray[0]);
        this.setBody(sanitisedArray[1]);
    }

}
